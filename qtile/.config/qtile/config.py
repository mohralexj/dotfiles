import os
import subprocess

from libqtile.config import EzKey as Key, EzClick as Click, EzDrag as Drag, Screen, Group
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook

from typing import List  # noqa: F401

# ===================================================================
# GENERAL
# ===================================================================
mod = "mod4"  # super key
groups = [Group(i) for i in "1234567890"]
dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
auto_fullscreen = True
focus_on_window_activation = "smart"
wmname = "LG3D"  # this makes java ui toolkits work

# ===================================================================
# STARTUP
# ===================================================================
@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~')
    subprocess.Popen(os.path.join(home, '.config/qtile/autostart.sh'))

# ===================================================================
# KEYBINDS
# ===================================================================
keys = [
    # Switch between windows in current stack pane
    Key("M-j", lazy.layout.down()),
    Key("M-k", lazy.layout.up()),
    Key("M-<space>", lazy.layout.next()),

    # Move windows up or down in current stack
    Key("M-C-j", lazy.layout.shuffle_down()),
    Key("M-C-k", lazy.layout.shuffle_up()),

    # Switch window focus to other pane(s) of stack
    Key("M-<Tab>", lazy.next_layout()),

    # Swap panes of split stack
    Key("M-S-<space>", lazy.layout.rotate()),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key("M-S-<Return>", lazy.layout.toggle_split()),

    # Applications
    Key("M-<Return>", lazy.spawn("alacritty")),
    Key("M-c", lazy.spawn("chromium")),

    # Toggle between different layouts as defined below
    Key("M-w", lazy.window.kill()),

    Key("M-C-r", lazy.restart()),
    Key("M-C-q", lazy.shutdown()),
    # Key("M-r", lazy.spawncmd()),
    Key("M-r", lazy.spawn("rofi -show run")),
]

# group keys
for i in groups:
    keys.extend([
        # switch to group
        Key(f"M-{i.name}", lazy.group[i.name].toscreen()),
        # move window to group
        Key(f"M-S-{i.name}", lazy.window.togroup(i.name)),
    ])

# Drag floating layouts.
mouse = [
    Drag("M-1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag("M-S-1", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click("M-2", lazy.window.bring_to_front())
]

# ===================================================================
# LAYOUTS
# ===================================================================
layout_defaults = dict(
    margin=4,
    border_focus='#bd93f9',
    border_normal='#14151b',
    border_width=2,
)
layouts = [
    # layout.Floating(**layout_defaults),
    # layout.Bsp(**layout_defaults),
    # layout.Columns(**layout_defaults),
    # layout.Matrix(**layout_defaults),
    layout.MonadTall(**layout_defaults),
    layout.Max(**layout_defaults),
    # layout.MonadWide(**layout_defaults),
    # layout.Slice(**layout_defaults),
    # layout.Stack(**layout_defaults),
    # layout.Tile(**layout_defaults),
    # layout.TreeTab(**layout_defaults),
    # layout.VerticalTile(**layout_defaults),
    # layout.Zoomy(**layout_defaults),
]
# floating window rules
floating_layout = layout.Floating(float_rules=[
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},  # gitk
    {'wmclass': 'makebranch'},  # gitk
    {'wmclass': 'maketag'},  # gitk
    {'wname': 'branchdialog'},  # gitk
    {'wname': 'pinentry'},  # GPG key password entry
    {'wmclass': 'ssh-askpass'},  # ssh-askpass
])

# ===================================================================
# BARS
# ===================================================================
widget_defaults = dict(
    font='DejaVu Sans Mono',
    fontsize=16,
    padding=5,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            widgets=[
                widget.GroupBox(
                    active='#bd93f9',
                    this_current_screen_border='#bd93f9',
                    other_screen_border='#caa9fa',
                    use_mouse_wheel=False,
                    highlight_method='line',
                    borderwidth=2,
                ),
                widget.Prompt(),
                widget.WindowName(),
                widget.Systray(),
                widget.CurrentLayout(),
                widget.Volume(),
                widget.Battery(),
                widget.Clock(format='%a %-d %b %Y %H:%M'),
            ],
            size=32,
            opacity=0.85,
        ),
    ),
]

