" ===================================================================
" plugins
" ===================================================================

" install vim-plug if not already
if empty(glob(stdpath('data') . '/site/autoload/plug.vim'))
    silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source ~/.config/nvim/init.vim
endif

call plug#begin(stdpath('data') . '/plugged')
Plug 'airblade/vim-rooter' " set vim root to directory containing known project-files
Plug 'tpope/vim-sensible'

" commands
Plug 'chrisbra/colorizer', {'on': 'ColorToggle'}
Plug 'easymotion/vim-easymotion'
Plug 'godlygeek/tabular'
Plug 'kien/ctrlp.vim'
Plug 'mileszs/ack.vim'
Plug 'terryma/vim-multiple-cursors'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'

" interface
Plug 'itchyny/lightline.vim'
Plug 'majutsushi/tagbar'
Plug 'scrooloose/nerdtree', {'on': 'NERDTreeToggle'}
Plug 'liuchengxu/vim-which-key'

" colors
" Plug 'flazz/vim-colorschemes'
Plug 'morhetz/gruvbox'
Plug 'shinchu/lightline-gruvbox.vim'

" dev
Plug 'dense-analysis/ale' " linting
" Plug 'neoclide/coc.nvim', {
"         \ 'branch': 'release',
"         \ 'do': ':CocInstall coc-python'
"         \ }

" languages
Plug 'lifepillar/pgsql.vim'
" Plug 'cespare/vim-toml', {'for': 'toml'}
Plug 'hail2u/vim-css3-syntax'
" Plug 'nvie/vim-flake8', {'for': 'py'}
Plug 'octol/vim-cpp-enhanced-highlight'
Plug 'tbastos/vim-lua', {'for': 'lua'}
" Plug 'tmhedberg/simpylfold', {'for': 'py'}
" Plug 'vim-scripts/django.vim'
" Plug 'vim-scripts/indentpython.vim'
Plug 'zig-lang/zig.vim', {'for': 'zig'}
call plug#end()

" ===================================================================
" config
" ===================================================================
set cmdheight=1
au FocusGained,BufEnter * checktime
set number relativenumber
set lazyredraw
set ignorecase
set smartcase
set wildmode=longest:full,full
set wildignore+=*.o,*~,*.pyc
set wildignore+=*/.git/*,*/.hg/*,*.DS_Store
set wildignore+=*env/*,*venv/*
set hidden " buffer becomes hidden when abandoned
set whichwrap=<,>,h,l
set mouse=a
set magic
set showmatch " show matching brackets
syntax on
filetype plugin indent on
set ffs=unix,dos,mac
set nobackup
set nowritebackup
set updatetime=300
set signcolumn=yes
set cursorline
set nowrap

set statusline^=%{coc#status()}

" no error bells
set noerrorbells
set novisualbell
set t_vb=
set tm=500

" Tabs
set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4
set smartindent
set timeoutlen=500

" Remember position in file
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

" Plugins
if executable('ag')
    let g:ctrlp_user_command = 'ag %s -l --no-color -g ""'
    let g:ackprg='ag --vimgrep'
endif
let g:ycm_autoclose_preview_window_after_completion=1
let python_highlight_all=1
let NERDTreeIgnore=['\.pyc$', '\~$']
let g:sql_type_default = 'pgsql'
let g:python3_host_prog = '/usr/bin/python3'

" Theme
colo gruvbox
let g:lightline = {
      \ 'colorscheme': 'gruvbox',
      \ }

" Filetypes
au BufNewFile,BufRead *.zig setlocal colorcolumn=100
au BufNewFile,BufRead *.py setlocal colorcolumn=100
au BufNewFile,BufRead *.html setlocal colorcolumn=100

" ===================================================================
" keybinds
" ===================================================================
let g:mapleader="\<space>"
let g:maplocalleader=","
map <leader>w :w<cr>
map <leader>q :q<cr>
" map <leader>g :YcmCompleter GoToDefinitionElseDeclaration<CR>
map <leader>t :NERDTreeToggle<cr>

" clear search
map <leader>/ :let @/ = ""<cr>

" open which-key
nnoremap <silent> <leader> :WhichKey '<space>'<cr>

" trigger completion with c-space
noremap <silent><expr> <c-space> coc#refresh()
nmap <silent> <leader>gd <Plug>(coc-definition)
nmap <silent> <leader>gr <Plug>(coc-references)

" use <cr> to confirm completion. '<C-g>u' means break undo chain at current
" position.
if exists("*complete_info")
    inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<cr>"
else
    inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<cr>"
endif

" move by visual lines
nnoremap <silent> j gj
nnoremap <silent> k gk

" window movement
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

