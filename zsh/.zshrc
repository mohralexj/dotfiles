# ========================
#         _              
# _______| |__  _ __ ___ 
# |_  / __| '_ \| '__/ __|
#  / /\__ \ | | | | | (__ 
# /___|___/_| |_|_|  \___|
# ========================
# -----------------------------------------------------------------------------
# ENV
# -----------------------------------------------------------------------------
export PATH="$HOME/.local/bin:$HOME/.npm/bin:$HOME/.yarn/bin:$PATH"
export ZSH="/home/amohr/.oh-my-zsh"
export PAGER="/usr/bin/less -ISr"
export LANG=en_US.UTF-8
export TERMINAL=/usr/bin/alacritty
export LESSHISTFILE="/dev/null"

if [[ -x /usr/bin/nvim ]]; then
    export EDITOR="/usr/bin/nvim"
    export SUDO_EDITOR="/usr/bin/nvim"
    export VISUAL=/usr/bin/nvim
else
    export EDITOR="/usr/bin/vim"
    export SUDO_EDITOR="/usr/bin/vim"
    export VISUAL=/usr/bin/nvim
fi

HISTFILE="$HOME/.cache/.zsh_history"
HISTSIZE=10000
SAVEHIST=10000

# -----------------------------------------------------------------------------
# oh-my-zsh
# -----------------------------------------------------------------------------
ZSH_CUSTOM="$HOME/.local/share/oh-my-zsh-custom/"
# Themes: https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="simple"
#ENABLE_CORRECTION="true"
DISABLE_UNTRACKED_FILES_DIRTY="true"
plugins=(
    fzf
    sudo
    zsh-completions
    zsh-syntax-highlighting
)
source $ZSH/oh-my-zsh.sh

# -----------------------------------------------------------------------------
# zsh
# -----------------------------------------------------------------------------
setopt autocd extendedglob
unsetopt beep
bindkey -e
# completions
zstyle :compinstall filename '$HOME/.zshrc'
autoload -Uz compinit
compinit
_comp_options+=(globdots)

# oh my zsh
export ZSH="$HOME/.oh-my-zsh"

# go up directories until python env is found, or we hit /
function find_pyvenv() {
    local dir=$(pwd)
    while true; do
        [ "$dir" = "/" ] && return
        if [ -d "$dir/env" ]; then
            echo "$dir/env"
            return
        else
            dir="$(dirname "$dir")"
        fi
    done
}
# find and activate python project virtual env
function activate_pyvenv() {
    # don't activate again.
    # return non-zero to break the chpwd_functions chain (don't need to
    # try to deactivate if its not active)
    [ ! -v $VIRTUAL_ENV ] && return 1

    local env="$(find_pyvenv)"
    [ "$env" ] && [ -G "$env" ] && . "$env/bin/activate"
}
# deactivate python project virtual env when leaving project
function deactivate_pyvenv() {
    [ ! -v $VIRTUAL_ENV ] && [ ! "$(find_pyvenv)" ] && deactivate
}
#precmd_functions=("activate_pyvenv" "deactivate_pyvenv")

# -----------------------------------------------------------------------------
# aliases
# -----------------------------------------------------------------------------
alias chmod='chmod -v'
alias cp="cp -r"
alias grep='grep --color=auto'
alias ip='ip -color=auto'
alias ll='ls -al'
alias ls='ls --color=auto'
alias mkdir='mkdir -pv'
alias vim='nvim'

# mkdir and cd in one command
function ccd() {
    mkdir -pv $1
    cd $1
}

# mercurial
alias hgb='hg bookmark'
alias hgc='hg commit'
alias hgd='hg diff'
alias hgf='hg fetch -m merge'
alias hgh='hg heads'
alias hgi='hg import --no-commit'
alias hgl='hg pull -u'
alias hgm='hg merge'
alias hgp='hg push'
alias hgs='hg status'
alias hgu='hg update'

