;==========================================================
;
;
;   ██████╗  ██████╗ ██╗  ██╗   ██╗██████╗  █████╗ ██████╗
;   ██╔══██╗██╔═══██╗██║  ╚██╗ ██╔╝██╔══██╗██╔══██╗██╔══██╗
;   ██████╔╝██║   ██║██║   ╚████╔╝ ██████╔╝███████║██████╔╝
;   ██╔═══╝ ██║   ██║██║    ╚██╔╝  ██╔══██╗██╔══██║██╔══██╗
;   ██║     ╚██████╔╝███████╗██║   ██████╔╝██║  ██║██║  ██║
;   ╚═╝      ╚═════╝ ╚══════╝╚═╝   ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝
;
;
;   To learn more about how to configure Polybar
;   go to https://github.com/polybar/polybar
;
;   The README contains a lot of information
;
;==========================================================

[global/wm]
margin-top = 0
margin-bottom = 0

[settings]
screenchange-reload = true

; module defaults
format-padding = 2
format-margin = 0

[colors]
background = #282a36
background-alt = #14151b
foreground = #e6e6e6
foreground-alt = #bfbfbf
primary = #50fa7b
secondary = #f1fa8c
black = #282a36
red = #ff5555
green = #50fa7b
yellow = #f1fa8c
blue = #bd93f9
magenta = #ff79c6
cyan = #8be9fd
white = #bfbfbf

[bar/i3bar]
width = 100%
height = 32
background = ${colors.background}
foreground = ${colors.foreground}
border-color = ${colors.blue}
border-top-size = 0
border-right-size = 0
border-bottom-size = 0
border-left-size = 0
module-margin-left = 1
module-margin-right = 1
tray-position = right

font-0 = Dejavu Sans:style=Regular:size=12;2
font-1 = Symbols Nerd Font:size=14;3
; separator = ∫
separator = 

modules-left = i3 xwindow
modules-center = 
modules-right = wlan eth pulseaudio battery date

[module/xwindow]
type = internal/xwindow
label = %title:0:30:...%

[module/xkeyboard]
type = internal/xkeyboard
blacklist-0 = num lock

format-prefix = " "
format-prefix-foreground = ${colors.foreground-alt}
format-prefix-underline = ${colors.secondary}

label-layout = %layout%
label-layout-underline = ${colors.secondary}

label-indicator-padding = 2
label-indicator-margin = 1
label-indicator-background = ${colors.secondary}
label-indicator-underline = ${colors.secondary}

[module/filesystem]
type = internal/fs
interval = 30

mount-0 = /
mount-1 = /home
spacing = 2

label-mounted = %mountpoint%: %used%/%total%
label-unmounted = %mountpoint% not mounted
label-unmounted-foreground = ${colors.foreground-alt}


[module/i3]
type = internal/i3
format = <label-state> <label-mode>
index-sort = true
wrapping-scroll = true
format-padding = 0

ws-icon-0 = 1;
ws-icon-1 = 2;
ws-icon-2 = 3;
ws-icon-3 = 4;
ws-icon-4 = 5;
ws-icon-5 = 6;
ws-icon-6 = 7;
ws-icon-7 = 8;
ws-icon-8 = 9;

; focused = Active workspace on focused monitor
label-focused = %icon%
label-focused-background = ${colors.background-alt}
label-focused-padding = 2
; unfocused = Inactive workspace on any monitor
label-unfocused = %icon%
label-unfocused-padding = ${self.label-focused-padding}
; visible = Active workspace on unfocused monitor
label-visible = %icon%
label-visible-background = ${self.label-focused-background}
label-visible-padding = ${self.label-focused-padding}
; urgent = Workspace with urgency hint set
label-urgent = %icon%
label-urgent-background = ${colors.red}
label-urgent-padding = ${self.label-focused-padding}


[module/mpd]
type = internal/mpd
format-online = <label-song> <icon-prev> <icon-stop> <toggle> <icon-next>

icon-prev = 
icon-stop = 
icon-play = 
icon-pause = 
icon-next = 

label-song-maxlen = 25
label-song-ellipsis = true

[module/xbacklight]
type = internal/xbacklight

format = <ramp> <label>
label = %percentage%%

bar-width = 10
bar-indicator = |
bar-indicator-foreground = #fff
bar-indicator-font = 2
bar-fill = ─
bar-fill-font = 2
bar-fill-foreground = #9f78e1
bar-empty = ─
bar-empty-font = 2
bar-empty-foreground = ${colors.foreground-alt}

ramp-0 = 
ramp-1 = 
ramp-2 = 
ramp-3 = 
ramp-4 = 
ramp-5 = 
ramp-6 = 

[module/backlight-acpi]
inherit = module/xbacklight
type = internal/backlight
card = intel_backlight

[module/cpu]
type = internal/cpu
interval = 2
format-prefix = " "
format-prefix-foreground = ${colors.foreground-alt}
format-underline = #f90000
label = %percentage:2%%

[module/memory]
type = internal/memory
interval = 2
format-prefix = " "
format-prefix-foreground = ${colors.foreground-alt}
format-underline = #4bffdc
label = %percentage_used%%

[module/wlan]
type = internal/network
interface = wlp3s0

format-connected = <ramp-signal> <label-connected>
label-connected = %essid%

; format-disconnected =
format-disconnected = <label-disconnected>
;label-disconnected = %ifname% disconnected
;label-disconnected-foreground = ${colors.foreground-alt}

ramp-signal-0 = 

[module/eth]
type = internal/network
interface = enp2s0
interval = 3.0

format-connected-prefix = " "
format-connected-prefix-foreground = ${colors.foreground-alt}
label-connected = %local_ip%

format-disconnected =
; format-disconnected = <label-disconnected>
;format-disconnected-underline = ${self.format-connected-underline}
label-disconnected = %ifname% disconnected
label-disconnected-foreground = ${colors.foreground-alt}

[module/date]
type = internal/date
interval = 5

date = " %F"

time = %H:%M

format-prefix = 

label = %date% %time%

[module/pulseaudio]
type = internal/pulseaudio

label-volume = %percentage%%
format-volume = <ramp-volume> <label-volume>

label-muted = "婢"

ramp-volume-0 = 奄
ramp-volume-1 = 奄
ramp-volume-2 = 奔
ramp-volume-3 = 奔
ramp-volume-4 = 墳
ramp-volume-5 = 墳
ramp-volume-6 = 墳

bar-volume-width = 10
bar-volume-foreground-0 = ${colors.green}
bar-volume-foreground-1 = ${colors.green}
bar-volume-foreground-2 = ${colors.green}
bar-volume-foreground-3 = ${colors.green}
bar-volume-foreground-4 = ${colors.green}
bar-volume-foreground-5 = ${colors.yellow}
bar-volume-foreground-6 = ${colors.red}
bar-volume-gradient = false
bar-volume-indicator = ▉
bar-volume-indicator-font = 2
bar-volume-fill = ▉
bar-volume-fill-font = 2
bar-volume-empty = ─
bar-volume-empty-font = 2
bar-volume-empty-foreground = ${colors.foreground-alt}

[module/alsa]
type = internal/alsa

format-volume = <label-volume> <bar-volume>
label-volume = VOL
label-volume-foreground = ${root.foreground}

format-muted-prefix = " "
format-muted-foreground = ${colors.foreground-alt}
label-muted = sound muted

bar-volume-width = 10
bar-volume-foreground-0 = ${colors.green}
bar-volume-foreground-1 = ${colors.green}
bar-volume-foreground-2 = ${colors.green}
bar-volume-foreground-3 = ${colors.green}
bar-volume-foreground-4 = ${colors.green}
bar-volume-foreground-5 = ${colors.yellow}
bar-volume-foreground-6 = ${colors.red}
bar-volume-gradient = false
bar-volume-indicator = |
bar-volume-indicator-font = 2
bar-volume-fill = ─
bar-volume-fill-font = 2
bar-volume-empty = ─
bar-volume-empty-font = 2
bar-volume-empty-foreground = ${colors.foreground-alt}

[module/battery]
type = internal/battery
battery = BAT0
adapter = AC
full-at = 98
time-format = %-H:%M

format-discharging = <ramp-capacity> <label-discharging>
format-charging = <animation-charging> <label-charging>

format-full-prefix = " "
format-full = <label-full>

ramp-capacity-0 = 
ramp-capacity-1 = 
ramp-capacity-2 = 
ramp-capacity-3 = 
ramp-capacity-4 = 
ramp-capacity-5 = 
ramp-capacity-6 = 
ramp-capacity-7 = 
ramp-capacity-8 = 
ramp-capacity-9 = 
label-discharging = %percentage%%

animation-charging-0 = 
animation-charging-1 = 
animation-charging-2 = 
animation-charging-3 = 
animation-charging-4 = 
animation-charging-5 = 
animation-charging-6 = 
animation-charging-framerate = 500

; vim:ft=dosini
