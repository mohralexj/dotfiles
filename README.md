# Dotfiles

**Important Notes:**

1. Remember to clone recursively (`git clone --recurse-submodules -j8 git@gitlab.com:mohralexj/dotfiles.git .dotfiles`)
2. Using GNU Stow to manage symlinks. See example below.

**Stow example:**

```bash
$ cd ~/.dotfiles # go to wherever this repo is
$ stow alacritty --target=$HOME # omit `--target` if this repo is directly inside of $HOME
```

**Picom with blur:**

* Use `picom-tyrone-git` in AUR
* run picom with `picom --experimental-backends --backend glx`

